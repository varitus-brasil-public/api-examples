# Exemplos API Varitus

Neste repositório estão disponibilizados vários exemplos de consumo da API Varitus, escritas nas mais diversas linguagens de programação. Para dar aquele _gás_ na hora que for iniciar o desenvolvimento da integração conosco. 

Quer saber como receber as instruções, e dados para se integrar conosco? Acesse nosso [site de integração](http://varitus.com.br/api/)


#### Sua linguagem não está aqui também ?

Simples! Você pode abrir uma **issue** para nós, e providenciaremos de mostrar o melhor meio de trabalhar conosco. :)

#### Eu me integrei com linguagem 'X' e quero compartilhar !

Faça um fork deste repositório, e nos mande um **PR**, ficaremos felizes de analisar o que você fez, e disponibilizar aos demais!
