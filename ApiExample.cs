﻿using System;
using System.Net;
using System.IO;

namespace VaritusAPI
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			string token = "{ \"token\": \"YOUR_TOKEN_HERE\" }";

			Console.Write (ConsumeApi(token));

		}

		static private string ConsumeApi(string token) {
			HttpWebRequest apiRequest = 
				WebRequest.Create("http://app.varitus.com.br/ejb-varitus/resources/hello/helloWorld") as HttpWebRequest;

			apiRequest.Method = "POST";
			apiRequest.ContentType = "application/json";
			apiRequest.ContentLength = token.Length;

			StreamWriter requestWriter = new StreamWriter (apiRequest.GetRequestStream ());
			requestWriter.Write (token);
			requestWriter.Close ();

			StreamReader responseReader = new StreamReader (apiRequest.GetResponse ().GetResponseStream ());

			string apiResponse = responseReader.ReadToEnd ();

			responseReader.Close ();
			apiRequest.GetResponse ().Close ();

			return apiResponse;
		}
	}
}
