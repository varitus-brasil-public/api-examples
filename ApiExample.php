<?php
/* METODO DE CONSUMO */

class VaritusApi {

	function callApi($url, $param, $header) {
		$init = curl_init();

		curl_setopt ($init, CURLOPT_URL, $url);
		curl_setopt ($init, CURLOPT_HTTPHEADER, $header);
		curl_setopt ($init, CURLOPT_POSTFIELDS, json_encode($param));

		$result = curl_exec($init);
		curl_close($init);
		return $result;
	}

}

 /* EXEMPLO CHAMADA AO METODO DE CONSUMO */

$url = "http://app.varitus.com.br/ejb-varitus/resources/hello/helloWorld";
$param = array('token' => 'YOUR_TOKEN_HERE');
$header = array('Content-Type: application/json');

$consumeApi =  new VaritusApi();
$result =  $consumeApi->callApi($url, $param, $header);
echo $result;

?>
