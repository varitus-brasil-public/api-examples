import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public static StringBuffer callApi( String url, String param, String header ) throws IOException {

	String inputLine;
	URL obj = new URL(url);

	HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	con.setRequestMethod  ("POST");
	con.setRequestProperty( "Content-Type", header );

	con.setDoOutput(true);
	DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	wr.writeBytes(param);
	wr.flush();
	wr.close();

	BufferedReader in = new BufferedReader ( new InputStreamReader ( con.getInputStream() ));

	StringBuffer response = new StringBuffer();

	while ( ( inputLine = in.readLine() ) != null) {
		  response.append(inputLine);
	}
	in.close();

	return response;
}

// Exemplo de chamada ao metodo de consumo.
//
private static String  token = "{ \"token\": \"YOUR_TOKEN_HERE\" }";
private static String  url = "http://app.varitus.com.br/ejb-varitus/resources/hello/helloWorld";
private static String  header = "application/json";

public static void main(String[] args) throws Exception {
	StringBuffer result = callApi(url, token, header);
	System.out.println(result.toString());
}             
