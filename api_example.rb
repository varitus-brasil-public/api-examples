require 'net/http'
require 'json'
require 'uri'

module VaritusAPI
  class Consumer

    attr_accessor :token
    attr_reader :response

    VARITUS_API = 'http://app.varitus.com.br/ejb-varitus/resources'

    def initialize(token)
      @token = token
    end

    def send
      uri = URI.parse(VARITUS_API + '/hello/helloWorld')
      http = Net::HTTP.new(uri.host, uri.port)	
      api_request = Net::HTTP::Post.new(uri.request_uri, 'Content-Type' => 'application/json; charset=utf8')
      api_request.body = { token: @token }.to_json
      @response =  http.request(api_request).body
    end

  end
end

api = VaritusAPI::Consumer.new('YOUR_TOKEN_HERE')
api.send
puts api.response


